# This code is from _____, who was instrumental in helping me move this tutorial to V2 of the API. 

import tweepy 
import config
import time
import re 

# V.2 tweepy Client endpoints
api_m = tweepy.Client(
    bearer_token = config.bearer_token,
    consumer_key = config.consumer_key,
    consumer_secret = config.consumer_secret,
    access_token = config.access_token,
    access_token_secret = config.access_token_secret
    )


#Global Veriable
METHOD = "Mention" # "Keyword"

# bot information
bot_id = 999999999999999999999 
bot_name = "@YourBotsTwitterHandle"

# the keyword(s) to search for if streaming only keywords rather than replying to mentions
keyword = "keyword1 OR keyword2"        # Otherwise, just set to "" empty string
keywordList = ["keyword1", "keyword2"]  # Otherwise, just set to [""] list with empty string

#########  https://developer.twitter.com/en/docs/twitter-api/tweets/filtered-stream/integrate/build-a-rule
# EXAMPLE keyword filters
# "snow day #NoSchool"                   match Tweets containing the terms snow and day and the hashtag #NoSchool.
# "grumpy OR cat OR #meme"               match any Tweets containing at least the terms grumpy or cat, or the hashtag #meme.
# "cat #meme -grumpy"                    match Tweets containing the hashtag #meme and the term cat, but only if they do not contain the term grumpy
# "(grumpy cat) OR (#meme has:images)"   match Tweets containing the terms grumpy and cat, or Tweets with images containing the hashtag #meme. Note that ANDs are applied first, then ORs are applied.

#########


# Regular expression to match a user twitter handle mention in a tweet that might contain the keywords (for replies only)
# For example, Tweet : "@IceCreamCone This is a sample tweet" would be a match on keyword "Cone" and we don't want that
# only use if replying to non mentions (Check Twitter's Policies on this)
pattern = f"@[^\s]*({'|'.join(keywordList)})[^\s]*"



# Sends a tweet
def tweeting_function():

    print("Sending Tweet!")
    message= "@Ferb, I know what we're going to do today!"
    api_m.create_tweet(text = message)
    print("Tweet Sent!")

#tweeting_function()



def commenting_function(twt):
    global METHOD


    # get streamed tweet id
    tweet_id = twt.id

    if METHOD == "Keyword":
        # get remove Twitter Hanndles that contain the keywords, use if not setting up auto reply to mentions
        tweet_text = re.replace(
            pattern = pattern,
            replacement = "",
            string = twt.text.lower()
            )
    else:
        # get tweet text and send to lowercase
        tweet_text = twt.text.lower()

    # get author id of tweet
    author_id = twt.author_id


    # define tweet message
    message= "@Ferb, I know what we're going to do today!"

    print("Tweet Found in stream!")
   
    print(f"TWEET:{tweet_text}")
    

    # if auther tweet isnt the same as the id of the bot
    if (author_id != bot_id):

        # check if at least one keyword in tweet text, will always be true if method is "Mention", Checking to see if after removing a handle, the keyword still exists
        if any([word in tweet_text for word in keywordList]):

            try:
                print("Attempting Comment")
                api_m.create_tweet(text = message, in_reply_to_tweet_id = tweet_id)
                print("TWEET SUCCESSFUL")
            except Exception as err:
                print(err)
                print("Tweet wasn't nice enough or there is an authorization issue")

        #else for if tweet contains a user handle that matches keyword 
        else:
            print("Tweet contains user handle with keyword")

    #else statement for posts by the bot itself. 
    else:
        print("Could not comment: RT, QT, or bot tweet")


# create streaming object
class MyStreamListener(tweepy.StreamingClient):

    # when the streaming connects, run this
    def on_connect(self):

        print("Connected")


    # when a tweet is collected, run this
    def on_tweet(self, twts):
        
        commenting_function(twts)

        # you can set a time limit until the bot tweets again    
        #time.sleep(120)           

    # if an error occurs, run this
    def on_error(self, status):
        print(status)



# create a stream object       
stream_m = MyStreamListener(
    wait_on_rate_limit= True,            # should always be true, stops Twitter from yelling at you for taking to many resources
    bearer_token= config.bearer_token    # authentication token
    )


# if any rules (filters) exist in the stream, delete them
[stream_m.delete_rules(rule) for rule in stream_m.get_rules().data]


########  RULE FOR REPLYING TO KEYWORD TWEETS
# add streaming rule                 NOT A RETWEET OR QUOTE  ENGLISH  KEYWORD
stream_m.add_rules(tweepy.StreamRule(f"-is:retweet -is:quote lang:en {keyword}"))


########  RULE FOR REPLYING TO MENTIONS
# # add streaming rule                 NOT A RETWEET OR QUOTE  ENGLISH  ADDRESSED TO YOUR BOT
# stream_m.add_rules(tweepy.StreamRule(f"-is:retweet -is:quote lang:en to:{bot_name}"))

# print out the rules for the stream
print("Rules", stream_m.get_rules())

# start the stream and include author_id as tweet field so we can make sure author_id != bot_id
stream_m.filter(tweet_fields=['author_id'])