---
title: What's happening in the code version 1?
nav_order: 3

---

# Code Breakdown v1

This breakdown of the code in `bot_v1.py` is to help everyone understand the intent, purpose, and effects of the code in this project. This code uses version 1 of the Twitter API. For version 2, see [this page](https://devops-education.gitlab.io/workshops/python-8-ball-workshop/course/version2/)

## Import statements and initial List. 

```python
import tweepy
import config
```
These import statements bring a configuration file called `config` into the `bot.py` file, as well as importing the tweepy API wrapper. `config` contains a method for accessing environment variables where Twitter API keys will be stored. We do this so we can protect the keys from others because those keys grant access to the Twitter account they are connected to. By using environment variables and a config file, we create layers of security for the bot. 

```python
print("Bot Active")

#This is that auth required to give the bot access to the API
auth = tweepy.OAuth1UserHandler(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret
)

api_m = tweepy.API(auth) 
```
I added a print statement to get started and let us know the code is running.
This code comes directly from Tweepy's [documentation](https://docs.tweepy.org/en/stable/authentication.html#oauth-1-0a-user-context) This authorization method, OAuth 1.0, gives us access to the account the keys connect with. It gives us a variable we called `api_m` that is used to gain access to all the API v1 endpoints throughout this workshop. 

```python
#input the id of the twitter account that will be acting as a bot as an int
bot_id = 1
#write tha name of your bot including the @ here. This variable will be used in a few places. 
bot_name = "@MetzinAround"
```

The above section is decalring two variables that will help us later to ensure that commenting works correctly. You can find your bot_id by searching [Tweeter ID](https://tweeterid.com/) for your twitter bot name. 

```python
def tweeting_function():
    #The message that will become a tweet
    print("Sending Tweet!")
    message = "This is an automated tweet!"
    api_m.update_status(status = message)

tweeting_function()
```
This section is where we finally see a function designed to tweet. We could just type `api_m.update_status("message for bot")` but putting it in a function allows for some more flexibility. Once again, we use a print to know the function was activated. Then, a message is defined that will become the tweet, and finally we use api_m.update_status to create a tweet that is sent out. The line after the function is defined we run it with no parameters. 

```python
def commenting_function(twt):
    #the id of the tweet that activated the function
    the_id = twt.id
    #the name of the author of the above tweet
    name = twt.author.screen_name
    #This will be the tweet that gets sent back to the author
    message = f"@{name} This is a reply tweet"

    print("Tweet Found in stream!")
    #twt.author.screen_name, etc, are taken from Twitter docs LINK HERE
    #PRinting the name and text of the tweet found in stream. 
    print(f"TWEET: {name} - {twt.text}")
    #checks to make sure the tweet is not from the bot itself, that it is not a quote tweet and that it is not a retweet. 
    if ((twt.author.id != bot_id) and not (hasattr(twt, "quoted_status")) and not (hasattr(twt, "retweeted_status"))):
        #twitter used to have shorter tweets. The new longer tweet is called an extended tweet. This try except checks to see if twt.extended_tweet has an attribute of "full_text". If it doesn't, the_tweet is twt.text. This is checked for containing the words in the if else ahead. 
        try:
            the_tweet = twt.extended_tweet["full_text"]
        except AttributeError:
            the_tweet = twt.text
        #tweet has "please" and the bot's name in it
        if ("please" in the_tweet) and (bot_name in the_tweet):
            #another try/except to see if we're able to comment. If not, an error is printed to the console.
            try:
                print("Attempting Comment")
                api_m.update_status(status = message, in_reply_to_status_id = the_id)
                print("TWEET SUCCESSFUL")
            except Exception as err:
                print(err)
        #else for whether the tweet has `please` in it and if the bot was tagged in the tweet.
        else:
            print("Tweet wasn't nice enough")
    #else statement for the logic that keeps this from firing on retweets, quote tweets, and posts by the bot itself. 
    else:
        print("Could not comment: RT, QT, or bot tweet")
#a subclass is required for a [Tweepy&Twitter stream](https://docs.tweepy.org/en/stable/streaming.html). We're creating a subclass of Stream called `MyStreamListener` with a parameter of tweepy.Stream. tweepy.Stream requires four keys to be passed in much like `auth` above. This subclass includes the function on_status which runs our commenting_function function.
class MyStreamListener(tweepy.Stream):
#on_status is what happens when a status ("tweet") comes into the stream
    def on_status(self, twts):
        commenting_function(twts)               
```
`commenting_function` is a function that allows the bot to reply to people under specified logic events. This is the most complicated part of the bot workshop and involves try/except and if/else statements as well as an important paramter and some key value pairs related to that parameter. This is the entire function and I have added comments to it to help with understanding each part. 

In order to reply to a tweet, we need the id of it. Twitter gives every status update, called a tweet, a unique ID number. The API requires that id in order to reply. Since we're replying to a tweet, we should have the handle of the author of the tweet we'll be replying to. The becomes the first part of our reply. Finally, we create a varaible called `message` that contains the text that we will tweet out as a reply. 

We print a note that a tweet has been found in the stream, followed by the author of the tweet and text of it. The first set of logic checks to see that the tweetis not a quote tweet, retweet, or a tweet from the bot itself. This keeps the bot from possibly entering a constant state of tweeting replies to itself or others. We do this through the `hassatrr` keyword which cheks if an object has a specific attribute in it. Since tweets are objects, we can do this to see if it is a QT or RT. Next, we try to see if the tweet is 280 characters long and make that the text we're working with. If it's an extended tweet (280 characters) we capture that text. If it's not, we work with the regular text attritbute.

The next logic checks if the user said `please` in their tweet and if they mentioned the bot in their tweet as well. As long as both of these are true, the reply tweet will happen in a try/except. We try to tweet and use except to capture any exceptions in this process. We finish off our two `ifs` from before with two `elses` that simply print the criteria that wasn't met. 

The last section requires a class creation that pulls in the bearer token and sets rate limit checks to true. Rate limits keep us from runing afoul of Twitter's rules on tweeting, and the bearer token allows us to open the stream to view all of the content being posted on twitter. Three functions exist inside the class: on_status, on_connect, and on_error. on_connect takes input on what to do when the stream first opens. In our case, we just print to the console. on-error handles errors so we can know why the stream quit. Finally, on_status is what happens when the stream find a tweet based on our search criteria defined later. In this case, it runs the reply function above. 
```python
#create instance of stream class and pass in the keys. 
stream_m = MyStreamListener(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret)

#use "follow" to RT a specific account (for example: if you want to RT only tweets from a specific account, use `follow`), use "track" for tweets that contain the string included here. Note that track must be an array. This begins the stream with the filter looking specifically at interactions with the bot listed in the list for `track`
stream_m.filter(track =[bot_name])
```

These comments in the code block explain the code. 



[Next Page](https://devops-education.gitlab.io/workshops/python-8-ball-workshop/course/version2/)