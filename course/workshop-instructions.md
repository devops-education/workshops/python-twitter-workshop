---
title: Workshop Instructions
nav_order: 5
---

# Methodology

This workshop is based on two styles of teaching programming: Live Coding modeling and the PRIMM method. These two are valuable in deepening understanding of code concepts in young developers and avoiding misconcep[tions about best practices regarding code. Feel free to take the concepts in this workshop and change them to fit your groups particular needs. It's expected that the presenter is familiar with Python, GitLab UI including [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html#getting-started-with-merge-requests), WebIDE, [cloning](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) and [forking](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) repos, [git commands](https://docs.gitlab.com/ee/topics/git/useful_git_commands.html#useful-git-commands), and troubleshooting common issues. 

## Getting Started

It's important that every participant have a GitLab account before starting. Make sure you've visited the [getting Started page](https://devops-education.gitlab.io//workshops/python-twitter-workshop/course/create-an-account/) to ensure you've signed up for the accounts you need and have set up your machine for using GitLab and git locally. 

## Process 

👷🏻 under construction

### New Project
1. Log in to GitLab and create a new project. This can be done by navigating to the Projects page and clicking the blue "new project" button in the upper right side of the page.  
1. Create a blank project and title it "Twitter Bot"
1. Ensure the namespace is the username
1. Make the project public
1. Choose "no deployment planned" for the deployment target dropdown menu
1. Ensure "Initialize repository with a README" is checked
1. Click "Create Project"

### WebIDE
1. In the project page, find the dropdown menu currently labeled, "Web IDE" and click it. 
1. Use the add new file button to add two files, `bot.py` and  `config.py`
1. Add one more file, but click `.gitignore` underneath the input box
1. A dropdown menu will appear near the top of the webIDE. Select `Python` from the templates. 
1. Use the blue commit button and add a commit mesage. Then commit to main. 


### Clone Repo, PIP, venv, and dependencies. 
1. Use the dropdown menu labeled "clone" and select whichever local version works for you. .ssh and https should both be available to you. For any errors, see our docs on [cloning a repository](image.png). Select or create a folder to store your repo in. This should be the same folder for all your repos on your machine. 
1. There are a few dependencies to install for this project. We will install them using pip. If you do not have python and pip already installed, please do so first. Here are some [instructions](https://packaging.python.org/en/latest/tutorials/installing-packages/)
1. Best practice involves creating a virtual environment for dependencies for projects. Here are [instructions](https://realpython.com/python-virtual-environments-a-primer/#how-can-you-work-with-a-python-virtual-environment) for Windows and MacOS. 
1. Once these are done, it's time to use the command line. For the purposes of this tutorial we are working in VSCode with the built in terminal. 
1. In VSCode, ensure your terminal is open on the bottom and type in the following commands. 
- `pip install tweepy`
- `pip install python-dotenv`
- `pip install config`
- `pip install schedule` 
Each should be followed by some computing and loading in the terminal. These commands install libraries we'll need for this bot. 
1. Open bot.py

### Code

During this section, you are writing or copying the code in either `bot_v1.py` or `bot_v2.py`. You'll also need `config.py`. The code is explained in the [version 1 code breakdown](https://devops-education.gitlab.io//workshops/python-twitter-workshop/course/code-breakdown/) and the [version 2 code breakdown](https://devops-education.gitlab.io//workshops/python-twitter-workshop/course/version2/) section. After the code is written, you still need to create one more file to be able to run the code. 

1. Create a new file in the repo called .env. Into this file input the following variables. 

```text
access_token=
access_token_secret=
consumer_key=
consumer_secret=
bearer_token=
```

These variables will hold the four keys from Twitter. The instructions for retrieving the keys are in the [getting Started page](https://devops-education.gitlab.io//workshops/python-twitter-workshop/course/create-an-account/). `consumer_key` and `consumer_secret` are called `api_key` and `api_secret` in the Twitter developer portal. Input the correct values using your keys for these variables. Save ALL your files. 

2. You should be able to run the bot with `python3 bot.py` typed into the terminal.
3. Check for errors. 
4. When ready, move on to adding the second function (`commenting_function()`). You can practice tweeting at your bot from your personal Twitter account or have a friend tweet at it for you to test it. 

## Video

[Here is a video walk through](https://www.youtube.com/watch?v=n8d29_kNjAg) created for UF Girls who Code. This video goes through the version 1 workshop as a walkthrough. 




