---
title: What's happening in the code version 2?
nav_order: 4

---

# Code Breakdown v2

This breakdown of the code in `bot_v2.py` is to help everyone understand the intent, purpose, and effects of the code in this project. This code uses version 2 of the Twitter API. For version 1, see [this page](https://devops-education.gitlab.io/workshops/python-8-ball-workshop/course/code-breakdown/)

## Import statements and initial List. 

```python
import tweepy
import config
import time
import schedule as sched
```
These import statements bring a configuration file called `config.py` into the `bot.py` file, as well as importing the Tweepy API wrapper. `config.py` contains a method for accessing environment variables where Twitter API keys will be stored. We do this so we can protect the keys from others because those keys grant access to the Twitter account they are connected to. By using environment variables and a config file, we create layers of security for the bot. We import `time` and `schedule` to use for scheduling tweets. I use the `as` statment to shorten the keyword to access `schedule`

```python
print("Bot Active")

#This is that V2 authentication required to give the bot access to the API
api_m = tweepy.Client(
    bearer_token = config.bearer_token,
    consumer_key = config.consumer_key,
    consumer_secret = config.consumer_secret,
    access_token = config.access_token,
    access_token_secret = config.access_token_secret
    )
```
I added a print statement to get started and let us know the code is running.
This code comes directly from Tweepy's [documentation](NEEDNEW LINK) This authorization is required for version 2 of the API and includes Oauth2.0 and 1.0. we'll use the variable `api_m` to access the endpoints we'll need for tweeting, replying, and more.

```python
#input the id of the Twitter account that will be acting as a bot as an int
bot_id = 1
#write tha name of your bot including the @ here. This variable will be used in a few places. 
bot_name = "@MetzinAround"
# list of terms to be looking for and creating rules about later
filtered_terms = [bot_name]
```

The above section is decalring two variables that will help us later to ensure that commenting works correctly. You can find your bot_id by searching [Tweeter ID](https://tweeterid.com/) for your Twitter bot name. finally, the filtered_terms variable will have a list of terms that will help us make rules later.

```python
def tweeting_function():
    # function to be called to send an original tweet
    print("Sending Tweet!")
    message = "This is an automated Tweet"
    api_m.create_tweet(text = message)
    print("Tweet Sent")

#uncommenting the next line means the bot will tweet automatically upon running the file.
#tweeting_function()
```
This section is where we finally see a function designed to tweet. We could just type `api_m.create_tweet("message for bot")` but putting it in a function allows for some more flexibility later on. Once again, we use a print to know the function was activated. Then, a message is defined that will become the tweet, and finally we use api_m.update_status to create a tweet that is sent out. The line after the function is defined runs the function with no parameters. It is commented out for now. Eventually, you can schedule a tweet using the import schedule statement from earlier.

```python
def commenting_function(twt):
    #the id of the tweet that activated the function
    the_id = twt.id
    #This will be the tweet that gets sent back to the author
    message = "Thanks for saying please!"
    #the text of the incoming tweet
    the_tweet = twt.text.lower()

    print("Tweet Found in stream!")
    #twt.author.screen_name, etc, are taken from Twitter docs LINK HERE
    #PRinting the name and text of the tweet found in stream. 
    print(f"TWEET: {twt.text}")
    #tweet has "please" and the bot's name in it
    if ("please" in the_tweet) and (bot_name in the_tweet):
        #another try/except to see if we're able to comment. If not, an error is printed to the console.
        try:
            print("Attempting Comment")
            api_m.create_tweet(text = message, in_reply_to_tweet_id = the_id)
            print("TWEET SUCCESSFUL")
        except Exception as err:
            print(err)
        #else for whether the tweet has `please` in it and if the bot was tagged in the tweet.
    else:
        print("Tweet wasn't nice enough")

#a subclass is required for a [Tweepy & Twitter stream](https://docs.tweepy.org/en/stable/streaming.html). We're creating a subclass of Stream called `MyStreamListener` with two parameters
class MyStreamListener(tweepy.StreamingClient):

    # on_tweet is what happens when a tweet comes into the stream
    def on_tweet(self, twts):
        commenting_function(twts)   
         
    # on connect says when the stream connects
    def on_connect(self):
        print("Stream Connected")

    # error handler
    def on_error(self, status):
        print(status)         
               
```
`commenting_function` is a function that allows the bot to reply to people under specified logic events. This is the most complicated part of the bot workshop and involves try/except and if/else statements as well as an important parameter and some key value pairs related to that parameter. This is the entire function and I have added comments to it to help with understanding each part. 

In order to reply to a tweet, we need the id of it. Twitter gives every status update, called a tweet, a unique ID number. The API requires that id in order to reply. We create a variable called `message` that contains the text that we will tweet out as a reply. 

We print a note that a tweet has been found in the stream, followed by the text of it.

The logic checks if the user said `please` in their tweet and if they mentioned the bot in their tweet as well. As long as both of these are true, the reply tweet will happen in a try/except. We try to tweet and use except to capture any exceptions or errors in this process. 

The last section requires a class creation that pulls in the tweepy streaming client. Rate limits keep us from runing afoul of Twitter's rules on tweeting, and the bearer token allows us to open the stream to view all of the content being posted on Twitter. Three functions exist inside the class: on_tweet, on_connect, and on_error. on_connect takes input on what to do when the stream first opens. In our case, we just print to the console. on_error handles errors so we can know why the stream quit. Finally, on_tweet is what happens when the stream finds a tweet based on our search criteria defined later. In this case, it runs the reply function above. 


```python
#create instance of stream class
stream_m = MyStreamListener(bearer_token= config.bearer_token, wait_on_rate_limit= True)

for term in filter_terms:
    stream_m.add_rules(tweepy.StreamRule(term))

stream_m.filter(expansions=["author_id"])
#use "follow" to RT a specific account (for example: if you want to RT only tweets from a specific account, use `follow`), use "track" for tweets that contain the string included here. Note that track must be an array


sched.every().day.at("12:00").do(tweeting_function)

# Turns on the schedule
while True:
  sched.run_pending()
  time.sleep(1)
```

We created a class earlier, but now we need a specific instance of that class that takes an input of our bearer token as well as a rate limit so we don't break our bot or make Twitter mad. We have a couple rules added to afect what we're looking at in the stream. First, we use a for loop to check our list `filter_terms` for keywords and adding rules that watch for them. In the default case of this code, it's looking for metnions of the bot's name for replies. We start the stream with `.filter()` and include the expansions keyword so we can return the author_id if we want to later. Next, we use the docs from [schedule](https://schedule.readthedocs.io/en/stable/) and wrote a line to make our bot tweet every day at noon. The final lines turn on the schedule using `.run_pending` and then uses `time.sleep()` to wait for the next time it runs. 

A note about threading, Python won't run these all concurrently, so you'll need to comment out either the schedule runner or the commenting code to make sure it works. I recommend starting with only the tweet on demand and schedule code and then trying just the commenting code. 



[Next Page](https://devops-education.gitlab.io/workshops/python-8-ball-workshop/course/workshop-instructions/)