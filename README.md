# GitLab Python Twitter Bot Workshop

This repository contains the Twitter bot Workshop created by the GitLab for Education team.

This workshop can be conducted in person or completed asynchronously.
